import React, { Component } from "react";
import { Card } from "components/Card/Card.jsx";
import { Grid, Row, Col } from "react-bootstrap";

export class HelpComponent extends Component {

    constructor() {
        super()
    }

    render() {
        return (
            <div>
                <Row>
                    <Col lg={100} sm={12}>
                        <Card
                            title="File Format"
                            content={
                                <div>
                                    There are 6 mandatory columns in the excel file to be uploaded, those columns are:
                                    <br></br>
                                    -Day - must be a number from 1 to 31
                                    <br></br>
                                    -Month - must be a number from 1 to 12
                                    <br></br>
                                    -Year - must be a number from 0 to 9999
                                    <br></br>
                                    -Time period - if there are 96 periods per day, the column must start from 1 (the beggining of a day) and go from 1-96
                                    <br></br>
                                    -Day of week - must be a number from 1 to 7
                                    <br></br>
                                    -A column with any name, that will represent the target values (usually consumption values) - the values must be numbers
                                    <br></br>
                                    <br></br>
                                    The excel can have other columns, so this web page allows you to write the desired target column, that could be any column except the first 5 referenced above
                                    <br></br>
                                    It's recommended that the excel file contains only one sheet
                                </div>
                            }
                        />
                    </Col>
                </Row>
                <Row>
                    <Col lg={100} sm={12}>
                        <Card
                            title="Historical Data Methods"
                            content={
                                <div>
                                    <div>Previous Days: this method only uses historical data of the same period of the day as the target,
                                     if the blindness periods are greater than the amount of periods in a day, at least one previous day will be skipped</div>
                                    <div>Neighbours: this method takes the nearest periods before the target, taking into account the blindness periods</div>
                                </div>
                            }
                        />
                    </Col>
                </Row>
                <Row>
                    <Col lg={100} sm={12}>
                        <Card
                            title="Forecast windows"
                            content={
                                <div>
                                    <div>A forecast window is the amount of historical data that will be used to train the neural network</div>
                                    <div>Choosing multiple windows will train the neural network multiple times and return different results for each one of these trainings</div>
                                </div>
                            }
                        />
                    </Col>
                </Row>
            </div>
        )
    }

}

export default HelpComponent