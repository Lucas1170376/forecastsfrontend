import React, { Component, useMemo, useCallback } from "react";
import { Grid, Row, Col } from "react-bootstrap";
import { Card } from "components/Card/Card.jsx";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

export class Results extends Component {

    constructor(props) {
        super(props)
        this.state = {

        }
        this.renderResults = this.renderResults.bind(this)
        this.renderResultsTrainEachPeriod = this.renderResultsTrainEachPeriod.bind(this)
        this.renderResultsTrainEachDay = this.renderResultsTrainEachDay.bind(this)
    }

    render() {
        return (
            <div>
                {
                    this.props.getResults() != null ?
                        <this.renderResults></this.renderResults>
                        :
                        null
                }
            </div>
        )
    }

    renderResults() {
        let trainingMethod = this.props.getTrainingMethod()
        if (trainingMethod == 'Train each period') {
            return this.renderResultsTrainEachPeriod()
        } else {
            return this.renderResultsTrainEachDay()
        }
    }

    renderResultsTrainEachPeriod() {
        let resultsList = this.props.getResults()
        let rows = []
        let day, dayOfWeek, month, year, dateString
        for (let resultObject of resultsList) {
            console.log(resultObject)
            let period = resultObject['time_period']
            day = resultObject['day']
            dayOfWeek = resultObject['day_of_week']
            month = resultObject['month']
            year = resultObject['year']
            dateString = String(day) + '/' + String(month) + '/' + String(year)
            let realValue = Math.round((resultObject['real_value'] + Number.EPSILON) * 100) / 100
            let results = resultObject['results']
            for (let result of results) {
                console.log(result)
                let hist_window = result['window']
                let resultListList = result['result']
                let resultValue = Math.round((resultListList[0][0] + Number.EPSILON) * 100) / 100
                let error = Math.abs(Math.round((resultValue - realValue + Number.EPSILON) * 100) / 100)
                rows.push({ 'period': period, 'window': hist_window, 'resultValue': resultValue, 'realValue': realValue, 'error': error })
            }
        }
        return (
            <div>
                <Card
                    title={"Results for " + dateString}
                    category={"Day of week: " + dayOfWeek}
                    content={
                        <TableContainer component={Paper}>
                            <Table size="medium" aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell align="center"> Time Period </TableCell>
                                        <TableCell align="center"> Window </TableCell>
                                        <TableCell align="center"> Prediction </TableCell>
                                        <TableCell align="center"> Real Value </TableCell>
                                        <TableCell align="center"> Error </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {rows.map((row) => (
                                        <TableRow key={row.name}>
                                            <TableCell component="th" scope="row">
                                                {row.period}
                                            </TableCell>
                                            <TableCell align="center">{row.window}</TableCell>
                                            <TableCell align="center">{row.resultValue}</TableCell>
                                            <TableCell align="center">{row.realValue}</TableCell>
                                            <TableCell align="center">{row.error}</TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    }
                />
            </div>
        )
    }

    renderResultsTrainEachDay() {
        let response = this.props.getResults()
        console.log(response)
        let rows = []

        let timePeriods = response['time_periods']
        let day = response['day']
        let dayOfWeek = response['day_of_week']
        let month = response['month']
        let year = response['year']

        let dateString = String(day) + '/' + String(month) + '/' + String(year)

        let realValues = response['real_values']
        let resultsList = response['results']
        for (let resultObject of resultsList) {
            let hist_window = resultObject['window']
            let results = resultObject['result']
            let size = results.length
            console.log(size)
            for (let i = 0; i < size; i++) {
                let timePeriod = timePeriods[i]
                let realValue = Math.round((realValues[i] + Number.EPSILON) * 100) / 100
                let result = Math.round((results[i][0] + Number.EPSILON) * 100) / 100
                let error = Math.abs(Math.round((result - realValue + Number.EPSILON) * 100) / 100)
                rows.push({ 'period': timePeriod, 'window': hist_window, 'resultValue': result, 'realValue': realValue, 'error': error })
            }
        }
        return (
            <div>
                <Card
                    title={"Results for " + dateString}
                    category={"Day of week: " + dayOfWeek}
                    content={
                        <TableContainer component={Paper}>
                            <Table size="medium" aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell align="center"> Time Period </TableCell>
                                        <TableCell align="center"> Window </TableCell>
                                        <TableCell align="center"> Prediction </TableCell>
                                        <TableCell align="center"> Real Value </TableCell>
                                        <TableCell align="center"> Error </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {rows.map((row) => (
                                        <TableRow key={row.name}>
                                            <TableCell component="th" scope="row" align="center">
                                                {row.period}
                                            </TableCell>
                                            <TableCell align="center">{row.window}</TableCell>
                                            <TableCell align="center">{row.resultValue}</TableCell>
                                            <TableCell align="center">{row.realValue}</TableCell>
                                            <TableCell align="center">{row.error}</TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    }
                />
            </div>
        )
    }

}
export default Results