import React, { Component, useMemo, useCallback } from "react";
import { Grid, Row, Col } from "react-bootstrap";
import { Card } from "components/Card/Card.jsx";
import { useDropzone } from 'react-dropzone';
import "components/FileUpload/Upload.css"
import { post } from 'axios';

export class FileUpload extends Component {

    constructor(props) {
        super(props)

        //todo add to env variable
        this.fileUploadURL = "http://127.0.0.1:8001/uploadFile/"

        this.state = {

        }
        this.StyledDropzone = this.StyledDropzone.bind(this)
        this.onFileUpload = this.onFileUpload.bind(this)
        this.onChangeFile = this.onChangeFile.bind(this)
        this.uploadFile = this.uploadFile.bind(this)
        this.onDropFile = this.onDropFile.bind(this)
    }

    onDropFile(file) {
        this.props.setFile(file[0])
    }

    onChangeFile(e) {
        this.props.setFile(e.target.files[0])
    }

    onFileUpload(e) {
        e.preventDefault()
        if (this.props.getFile() == null) {
            this.props.setFileUploadMessage("Please upload an excel file")
            return
        }
        this.props.setFileUploadMessage("Waiting for the server response")
        this.uploadFile().then((response) => {
            console.log("RESPONSE FROM THE SERVER")
            console.log(response.data)
            let data = response.data
            let pathToFile = data.pathToFile
            let columns = data.columns
            let timeInfo = data.timeInfo
            this.props.setTimeInfo(timeInfo)
            this.props.setColumns(columns)
            this.props.setFilename(pathToFile)
            this.props.setFileUploadMessage("Upload successful")
        }).catch(error => {
            this.props.setFileUploadMessage("Something went wrong")
        })
    }

    uploadFile() {
        let formData = new FormData()
        formData.append('file', this.props.getFile())
        let config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        return post(this.fileUploadURL, formData, config)
    }

    render() {
        return (<div>
            <Grid fluid>
                <Row>
                    <Col lg={100} sm={12}>
                        <div>
                            <br/>
                            <Card
                                title="Upload file"
                                category="Upload an excel(.xlsx) file"
                                content={
                                    <div>
                                        <this.StyledDropzone></this.StyledDropzone>
                                        <button onClick={this.onFileUpload}> Upload File </button>
                                        {this.props.getFileUploadMessage() != null ? <div>{this.props.getFileUploadMessage()}</div> : null}
                                    </div>
                                }
                            />
                        </div>
                    </Col>
                </Row>
            </Grid>
        </div>)
    }

    StyledDropzone(props) {
        const baseStyle = {
            flex: 1,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            padding: '20px',
            borderWidth: 2,
            borderRadius: 2,
            borderColor: '#eeeeee',
            borderStyle: 'dashed',
            backgroundColor: '#fafafa',
            color: '#bdbdbd',
            outline: 'none',
            transition: 'border .24s ease-in-out'
        };

        const activeStyle = {
            borderColor: '#2196f3'
        };

        const acceptStyle = {
            borderColor: '#00e676'
        };

        const rejectStyle = {
            borderColor: '#ff1744'
        };

        const onDrop = useCallback(acceptedFiles => {
            this.onDropFile(acceptedFiles)
        }, [])

        const { acceptedFiles, getRootProps, getInputProps, isDragAccept, isDragReject, isDragActive } = useDropzone({
            onDrop
        });

        const style = useMemo(() => ({
            ...baseStyle,
            ...(isDragActive ? activeStyle : {}),
            ...(isDragAccept ? acceptStyle : {}),
            ...(isDragReject ? rejectStyle : {})
        }), [
            isDragActive,
            isDragReject
        ]);

        return (
            <section className="container">
                <div {...getRootProps({ className: 'dropzone', style })}>
                    <input {...getInputProps()} onChange={this.onChangeFile} />
                    <p>Drag 'n' drop some files here, or click to select files</p>
                </div>
                <aside>
                    <h4>Selected file:</h4>
                    <ul>
                        {this.props.getFile() != null ? (this.props.getFile().name) : null}
                    </ul>
                </aside>
            </section>
        );
    }

}

export default FileUpload