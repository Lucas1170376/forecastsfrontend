import React, { Component, useMemo, useCallback } from "react";
import { Grid, Row, Col } from "react-bootstrap";
import { Card } from "components/Card/Card.jsx";
import { post } from 'axios';

import Slider from '@material-ui/core/Slider';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

import TextField from '@material-ui/core/TextField';

export class ForecastConfiguration extends Component {

    constructor(props) {
        super(props)
        this.state = {
            targetColumn: null,
            trainingMethod: null,
            results: null,
            selectedYear: null,
            selectedMonth: null,
            selectedDay: null,
            selectedPeriod: null,
            considerWeekdays: null,
            blindnessPeriods: null,
            method: null,
            windows: null,
            allPeriods: null,
            forecastConfigurationMessage: null,
        }
        this.setTargetColumn = this.setTargetColumn.bind(this)
        this.renderTargetColumnRadio = this.renderTargetColumnRadio.bind(this)
        this.renderYearSelect = this.renderYearSelect.bind(this)
        this.renderMonthSelect = this.renderMonthSelect.bind(this)
        this.renderDaySelect = this.renderDaySelect.bind(this)
        this.renderPeriodSelect = this.renderPeriodSelect.bind(this)
        this.renderHistoricalDataMethod = this.renderHistoricalDataMethod.bind(this)
        this.renderConsiderWeekdays = this.renderConsiderWeekdays.bind(this)
        this.renderBlindnessPeriods = this.renderBlindnessPeriods.bind(this)
        this.onRequestForecast = this.onRequestForecast.bind(this)
        this.forecast = this.forecast.bind(this)
        this.renderWindowSelect = this.renderWindowSelect.bind(this)
        this.renderTrainingMethod = this.renderTrainingMethod.bind(this)
        this.renderUseAllPeriodsButton = this.renderUseAllPeriodsButton.bind(this)

        this.getSelectedYear = this.getSelectedYear.bind(this)
        this.setSelectedYear = this.setSelectedYear.bind(this)
        this.getSelectedMonth = this.getSelectedMonth.bind(this)
        this.setSelectedMonth = this.setSelectedMonth.bind(this)
        this.getSelectedDay = this.getSelectedDay.bind(this)
        this.setSelectedDay = this.setSelectedDay.bind(this)
        this.getSelectedPeriod = this.getSelectedPeriod.bind(this)
        this.setSelectedPeriod = this.setSelectedPeriod.bind(this)
        this.getConsiderWeekdays = this.getConsiderWeekdays.bind(this)
        this.setConsiderWeekdays = this.setConsiderWeekdays.bind(this)
        this.getWindows = this.getWindows.bind(this)
        this.setWindows = this.setWindows.bind(this)
        this.getBlindnessPeriods = this.getBlindnessPeriods.bind(this)
        this.setBlindnessPeriods = this.setBlindnessPeriods.bind(this)
        this.getMethod = this.getMethod.bind(this)
        this.setMethod = this.setMethod.bind(this)
        this.toggleUseAllPeriods = this.toggleUseAllPeriods.bind(this)
        this.getForecastConfigurationMessage = this.getForecastConfigurationMessage.bind(this)
        this.setForecastConfigurationMessage = this.setForecastConfigurationMessage.bind(this)
        this.getAllPeriods = this.getAllPeriods.bind(this)
        this.setAllPeriods = this.setAllPeriods.bind(this)
        this.getTrainingMethod = this.getTrainingMethod.bind(this)
        this.setTrainingMethod = this.setTrainingMethod.bind(this)
        this.onSliderChange = this.onSliderChange.bind(this)
    }
    
    getTrainingMethod(){
        return this.state.trainingMethod
    }

    setTrainingMethod(e){
        this.setState({trainingMethod: e.target.value})
    }

    getSelectedYear() {
        return this.state.selectedYear
    }

    setSelectedYear(e) {
        e.preventDefault()
        this.setState({ selectedYear: e.target.value })
    }

    getSelectedMonth() {
        return this.state.selectedMonth
    }

    setSelectedMonth(e) {
        e.preventDefault()
        this.setState({ selectedMonth: e.target.value })
    }

    getSelectedDay() {
        return this.state.selectedDay
    }

    setSelectedDay(e) {
        e.preventDefault()
        this.setState({ selectedDay: e.target.value })
    }

    getSelectedPeriod() {
        return this.state.selectedPeriod
    }

    setSelectedPeriod(e) {
        e.preventDefault()
        this.setState({ selectedPeriod: Number(e.target.value) })
    }

    getConsiderWeekdays() {
        return this.state.considerWeekdays
    }

    setConsiderWeekdays(e) {
        e.preventDefault()
        this.setState({ considerWeekdays: e.target.value })
    }

    getBlindnessPeriods() {
        return this.state.blindnessPeriods
    }

    setBlindnessPeriods(e) {
        e.preventDefault()
        this.setState({ blindnessPeriods: e.target.value })
    }

    getMethod() {
        return this.state.method
    }

    setMethod(e) {
        e.preventDefault()
        this.setState({ method: e.target.value })
    }

    getWindows() {
        return this.state.windows
    }

    setWindows(windows) {
        this.setState({ windows: windows })
    }

    toggleUseAllPeriods() {
        if (this.state.selectedPeriod == 'All periods') {
            this.setState({ selectedPeriod: null })
        } else {
            this.setState({ selectedPeriod: 'All periods' })
        }
    }

    getForecastConfigurationMessage() {
        return this.state.forecastConfigurationMessage
    }

    setForecastConfigurationMessage(m) {
        this.setState({ forecastConfigurationMessage: m })
    }

    getAllPeriods() {
        return this.state.allPeriods
    }

    setAllPeriods(e) {
        this.setState({ allPeriods: e })
    }

    setTargetColumn(e) {
        e.preventDefault()
        this.setState({
            targetColumn: e.target.value
        })
    }

    onRequestForecast() {
        let year = this.state.selectedYear
        if (year == null) {
            this.setState({ forecastConfigurationMessage: "Select a year"} )
            this.props.setForecastConfigurationMessage("Select a year")
            return
        }
        let month = this.state.selectedMonth
        if (month == null) {
            this.setState({ forecastConfigurationMessage: "Select a month"} )
            return
        }
        let day = this.state.selectedDay
        if (day == null) {
            this.setState({ forecastConfigurationMessage: "Select a day"} )
            return
        }
        let periodList = []
        let selectedPeriod = this.state.selectedPeriod
        if (selectedPeriod == null) {
            this.setState({ forecastConfigurationMessage: "Select a period"} )
            return
        }
        if (selectedPeriod == 'All periods') {
            periodList = this.state.allPeriods
        } else {
            periodList.push(selectedPeriod)
        }
        let method = this.state.method
        if (method == null) {
            this.setState({ forecastConfigurationMessage: "Select a method"} )
            return
        }
        let considerWeekdays = this.state.considerWeekdays
        if (considerWeekdays == null) {
            this.setState({ forecastConfigurationMessage: "Choose if to consider weekdays / weekends only"} )
            return
        }
        if (considerWeekdays == "Yes") {
            considerWeekdays = "true"
        }
        let blindnessPeriods = this.state.blindnessPeriods
        if (blindnessPeriods == null || isNaN(blindnessPeriods)) {
            this.setState({ forecastConfigurationMessage: "Blindness periods must be an integer"} )
            return
        }
        if (!Number.isInteger(Number(blindnessPeriods))) {
            this.setState({ forecastConfigurationMessage: "Blindness periods must be an integer"} )
            return
        }
        let targetColumn = this.state.targetColumn
        if (targetColumn == null) {
            this.setState({ forecastConfigurationMessage: "Please select a target column"} )
            return
        }
        let filename = this.props.getFilename()
        let windows = this.state.windows
        let minWindow = windows[0]
        let maxWindow = windows[1]
        let trainingMethod = this.state.trainingMethod
        this.props.setTrainingMethod(trainingMethod)
        if (trainingMethod == null) {
            this.setState({ forecastConfigurationMessage: "Please choose a training method"} )
            return
        }

        let request = {}
        request['filename'] = filename
        request['targetColumn'] = targetColumn
        request['blindnessPeriods'] = blindnessPeriods
        request['historicalDataMethod'] = method
        request['weekdayConsideration'] = considerWeekdays
        request['targetPeriods'] = periodList
        request['targetDay'] = day
        request['targetMonth'] = month
        request['targetYear'] = year
        request['minWindow'] = minWindow
        request['maxWindow'] = maxWindow
        request['trainingMethod'] = trainingMethod

        this.forecast(request).then((response) => {
            console.log(response)
            let message = response.data.message
            if(message == 'Failure'){
                this.setState({ forecastConfigurationMessage: response.data.errors.error} )
                return
            }
            let results = response.data.results
            this.props.setResults(results)
            this.setState({ forecastConfigurationMessage: "Results returned from the server successfully"} )
        })

    }

    forecast(data) {
        this.setState({ forecastConfigurationMessage: "Waiting for a server response"} )
        return post("http://127.0.0.1:8001/forecastOnePeriod/", data)
    }

    render() {
        return (<div>
            <Grid fluid>
                <Row>
                    <Col lg={100} sm={12}>
                        <this.renderTargetColumnRadio />
                        <this.renderYearSelect />
                        <this.renderMonthSelect />
                        <this.renderDaySelect />
                        <this.renderPeriodSelect />
                        <this.renderBlindnessPeriods />
                        <this.renderHistoricalDataMethod />
                        <this.renderConsiderWeekdays />
                        <this.renderWindowSelect />
                        <this.renderTrainingMethod />
                        {this.props.getColumns() != null ?
                            <button onClick={this.onRequestForecast}>Request Forecast</button> : null
                        }
                        <br/>
                        {this.state.forecastConfigurationMessage}
                    </Col>
                </Row>
            </Grid>
        </div>)
    }

    renderTargetColumnRadio() {
        let columns = this.props.getColumns()
        if (columns == null) {
            return (
                <div>
                    Please upload a file
                </div>
            )
        }

        return (
            <Card
                title="Target Column"
                category="Choose the column from the file from which you want to perform the forecast"
                content={
                    <div>
                        <FormControl>
                            <FormLabel></FormLabel>
                            <RadioGroup aria-label="targetColumn" name="targetColumn" value={this.state.targetColumn} onChange={this.setTargetColumn}>
                                {
                                    columns.map((column) => {
                                        return (
                                            <FormControlLabel value={column} control={<Radio />} label={column} />
                                        )
                                    })
                                }
                            </RadioGroup>
                        </FormControl>
                    </div>
                }
            />

        )
    }

    renderYearSelect() {
        let timeInfo = this.props.getTimeInfo()
        if (timeInfo == null) {
            return null
        }

        let years = Object.keys(timeInfo)

        return (
            <div>
                <Card
                    title="Select Year"
                    category="Choose the year to forecast"
                    content={
                        <div>
                            <FormControl>
                                <FormLabel></FormLabel>
                                <RadioGroup row aria-label="targetYear" name="targetYear" value={this.state.selectedYear} onChange={this.setSelectedYear}>
                                    {
                                        years.map((year) => {
                                            return (
                                                <FormControlLabel value={year} control={<Radio />} label={year} />
                                            )
                                        })
                                    }
                                </RadioGroup>
                            </FormControl>
                        </div>
                    }
                />
            </div>
        )

    }

    renderMonthSelect() {
        let timeInfo = this.props.getTimeInfo()
        if (timeInfo == null) {
            return null
        }

        let year = this.state.selectedYear
        if (year == null) {
            return null
        }

        if ((timeInfo[year]) == undefined) {
            return null
        }
        let months = Object.keys(timeInfo[year])

        return (
            <Card
                title="Select Month"
                category="Choose the month to forecast"
                content={
                    <div>
                        <FormControl>
                            <FormLabel></FormLabel>
                            <RadioGroup row aria-label="targetMonth" name="targetMonth" value={this.state.selectedMonth} onChange={this.setSelectedMonth}>
                                {
                                    months.map((month) => {
                                        return (
                                            <FormControlLabel value={month} control={<Radio />} label={month} />
                                        )
                                    })
                                }
                            </RadioGroup>
                        </FormControl>
                    </div>
                }
            />
        )
    }

    renderDaySelect() {
        let timeInfo = this.props.getTimeInfo()
        if (timeInfo == null) {
            return null
        }

        let year = this.state.selectedYear
        if (year == null) {
            return null
        }

        let month = this.state.selectedMonth
        if (month == null) {
            return null
        }

        if ((timeInfo[year][month]) == undefined) {
            return null
        }
        let days = Object.keys(timeInfo[year][month])

        return (
            <Card
                title="Select Day"
                category="Choose the day to forecast"
                content={
                    <div>
                        <FormControl>
                            <FormLabel component="legend"></FormLabel>
                            <RadioGroup row aria-label="targetDay" name="targetDay" value={this.state.selectedDay} onChange={this.setSelectedDay}>
                                {
                                    days.map((day) => {
                                        return (
                                            <FormControlLabel value={day} control={<Radio />} label={day} />
                                        )
                                    })
                                }
                            </RadioGroup>
                        </FormControl>
                    </div>
                }
            />
        )
    }

    renderPeriodSelect() {
        let timeInfo = this.props.getTimeInfo()
        if (timeInfo == null) {
            return null
        }
        let year = this.state.selectedYear
        if (year == null) {
            return null
        }

        let month = this.state.selectedMonth
        if (month == null) {
            return null
        }

        let day = this.state.selectedDay
        if (day == null) {
            return null
        }

        if (timeInfo[year][month] == undefined) {
            return null
        }

        let dayObject = timeInfo[year][month][day]
        if (dayObject == undefined) {
            return null
        }

        let periods = dayObject['timePeriods']
        if (this.state.allPeriods != periods) {
            this.setAllPeriods(periods)
        }

        return (
            <Card
                title="Select Period"
                category="Choose the period to forecast"
                content={
                    <div>
                        {/*
                        <InputRange
                            maxValue={periods[periods.length - 1]}
                            minValue={periods[0]}
                            value={this.state.selectedPeriod}
                            onChange={value => this.setState({ selectedPeriod: value })}
                        />
                        */}
                        <FormControl>
                            <FormLabel component="legend"></FormLabel>
                            <RadioGroup row aria-label="targetPeriod" name="targetPeriod" value={this.state.selectedPeriod} onChange={this.setSelectedPeriod}>
                                {
                                    periods.map((period) => {
                                        return (
                                            <FormControlLabel value={period} control={<Radio />} label={period} disabled={this.state.selectedPeriod==='All periods'} />
                                        )
                                    })
                                }
                            </RadioGroup>
                        </FormControl>
                        <this.renderUseAllPeriodsButton />
                    </div>
                }
            />
        )
    }

    renderUseAllPeriodsButton(){
        if(this.state.selectedPeriod != 'All periods'){
            return(<button onClick={this.toggleUseAllPeriods}> Use all periods </button>)
        } else {
            return(<button onClick={this.toggleUseAllPeriods}> Don't use all periods </button>)
        }
    }

    renderConsiderWeekdays() {
        let timeInfo = this.props.getTimeInfo()
        if (timeInfo == null) {
            return null
        }
        let year = this.state.selectedYear
        if (year == null) {
            return null
        }

        let month = this.state.selectedMonth
        if (month == null) {
            return null
        }

        let day = this.state.selectedDay
        if (day == null) {
            return null
        }

        if (timeInfo[year][month] == undefined) {
            return null
        }

        let dayObject = timeInfo[year][month][day]
        if (dayObject == undefined) {
            return null
        }

        let weekday = Number(dayObject['weekday'])
        if (weekday >= 6) {
            return (
                <Card
                    title="Consider only weekends as historical data?"
                    category="The period you chose is on a weekend, do you want to only consider weekends as historical data?"
                    content={
                        <div>
                            <FormControl>
                                <FormLabel component="legend"></FormLabel>
                                <RadioGroup row aria-label="weekend" name="weekend" value={this.state.considerWeekdays} onChange={this.setConsiderWeekdays}>
                                    <FormControlLabel value={"Yes"} control={<Radio />} label={"Yes"} />
                                    <FormControlLabel value={"No"} control={<Radio />} label={"No"} />
                                </RadioGroup>
                            </FormControl>
                        </div>
                    }
                />
            )
        } else {
            return (
                <Card
                    title="Consider only weekdays as historical data?"
                    category="The period you chose is a weekday, do you want to only consider weekdays as historical data?"
                    content={
                        <div>
                            <FormControl>
                                <FormLabel component="legend"></FormLabel>
                                <RadioGroup row aria-label="weekend" name="weekend" value={this.state.considerWeekdays} onChange={this.setConsiderWeekdays}>
                                    <FormControlLabel value={"Yes"} control={<Radio />} label={"Yes"} />
                                    <FormControlLabel value={"No"} control={<Radio />} label={"No"} />
                                </RadioGroup>
                            </FormControl>
                        </div>
                    }
                />
            )
        }
    }

    renderBlindnessPeriods() {
        let columns = this.props.getColumns()
        if (columns == null) {
            return null
        }

        return (
            <Card
                title="Enter the blindness periods"
                category="These are the amount of periods where the system is blind, for example, if forecasting for period 19 and blindness periods
                = 2, historical data will only be obtained from period 16 and backwards"
                content={
                    <div>
                        <input type="text" onChange={this.setBlindnessPeriods} />
                    </div>
                }
            />
        )
    }

    renderHistoricalDataMethod() {
        let columns = this.props.getColumns()
        if (columns == null) {
            return null
        }

        return (
            <Card
                title="Enter the historical data method"
                category="Choose one"
                content={
                    <div>
                        <FormControl>
                            <FormLabel component="legend"></FormLabel>
                            <RadioGroup row aria-label="method" name="method" value={this.state.method} onChange={this.setMethod}>
                                <FormControlLabel value={"PreviousDays"} control={<Radio />} label={"PreviousDays"} />
                                <FormControlLabel value={"Neighbors"} control={<Radio />} label={"Neighbors"} />
                            </RadioGroup>
                        </FormControl>
                    </div>
                }
            />
        )
    }

    renderTrainingMethod() {
        let columns = this.props.getColumns()
        if (columns == null) {
            return null
        }

        return (
            <Card
                title="Enter the training method"
                category="Training each period means the neural network will use only the same periods of the day to train the network, while the other option
                trains the neural network using values of the whole day."
                content={
                    <div>
                        <FormControl>
                            <FormLabel component="legend"></FormLabel>
                            <RadioGroup row aria-label="trainingMethod" name="trainingMethod" value={this.state.trainingMethod} onChange={this.setTrainingMethod}>
                                <FormControlLabel value={"Train each period"} control={<Radio />} label={"Train each period"} />
                                <FormControlLabel value={"Train each day"} control={<Radio />} label={"Train each day"} />
                            </RadioGroup>
                        </FormControl>
                    </div>
                }
            />
        )
    }

    renderWindowSelect() {
        let columns = this.props.getColumns()
        if (columns == null) {
            return null
        }

        if (this.state.windows == null){
            this.setWindows([2,10])
        }

        return (
            <Card
                title="Select the forecast windows to be used"
                content={
                    <Slider
                        min={2}
                        max={10}
                        value={this.state.windows ? this.state.windows:[2,10]}
                        onChange={this.onSliderChange}
                        valueLabelDisplay="auto"
                        aria-labelledby="range-slider"
                        step={1}
                    />
                }
            />
        )
    }

    onSliderChange(e, value) {
        this.setState({ 'windows': value })
    }

}

export default ForecastConfiguration