/*!

=========================================================
* Light Bootstrap Dashboard React - v1.3.0
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import NotificationSystem from "react-notification-system";
import { Helmet } from "react-helmet";
import AdminNavbar from "components/Navbars/AdminNavbar";
import Footer from "components/Footer/Footer";
import Sidebar from "components/Sidebar/Sidebar";
import FixedPlugin from "components/FixedPlugin/FixedPlugin.jsx";

import { style } from "variables/Variables.jsx";

import routes from "routes.js";

class Admin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //file upload
            file: null,
            fileUploadMessage: null,
            filename: null,
            columns: null,
            timeInfo: null,
            //forecast config
            targetColumn: null,
            trainingMethod: null,
            results: null,
            //color
            color: 'blue'
        };

        this.getFile = this.getFile.bind(this)
        this.setFile = this.setFile.bind(this)
        this.getFileUploadMessage = this.getFileUploadMessage.bind(this)
        this.setFileUploadMessage = this.setFileUploadMessage.bind(this)
        this.getFilename = this.getFilename.bind(this)
        this.setFilename = this.setFilename.bind(this)
        this.getColumns = this.getColumns.bind(this)
        this.setColumns = this.setColumns.bind(this)
        this.getTimeInfo = this.getTimeInfo.bind(this)
        this.setTimeInfo = this.setTimeInfo.bind(this)
        this.getTargetColumn = this.getTargetColumn.bind(this)
        this.setTargetColumn = this.setTargetColumn.bind(this)
        this.getTrainingMethod = this.getTrainingMethod.bind(this)
        this.setTrainingMethod = this.setTrainingMethod.bind(this)
        this.getResults = this.getResults.bind(this)
        this.setResults = this.setResults.bind(this)
        
    }

    getFile() {
        return this.state.file
    }

    setFile(file) {
        this.setState({ file: file })
    }

    getFileUploadMessage() {
        return this.state.fileUploadMessage
    }

    setFileUploadMessage(fileUploadMessage) {
        this.setState({ fileUploadMessage: fileUploadMessage })
    }

    getFilename() {
        return this.state.filename
    }

    setFilename(filename) {
        this.setState({ filename: filename })
    }

    getColumns() {
        return this.state.columns
    }

    setColumns(columns) {
        this.setState({ columns: columns })
    }

    getTimeInfo() {
        return this.state.timeInfo
    }

    setTimeInfo(timeInfo) {
        this.setState({ timeInfo: timeInfo })
    }

    getTargetColumn() {
        return this.state.targetColumn
    }

    setTargetColumn(targetColumn) {
        this.setState({ targetColumn: targetColumn })
    }

    getTrainingMethod() {
        return this.state.trainingMethod
    }

    setTrainingMethod(e) {
        this.setState({ trainingMethod: e })
    }

    getResults() {
        return this.state.results
    }

    setResults(results) {
        this.setState({ results: results })
    }

    handleNotificationClick = position => {
        var color = Math.floor(Math.random() * 4 + 1);
        var level;
        switch (color) {
            case 1:
                level = "success";
                break;
            case 2:
                level = "warning";
                break;
            case 3:
                level = "error";
                break;
            case 4:
                level = "info";
                break;
            default:
                break;
        }
    };

    getRoutes = routes => {
        return routes.map((prop, key) => {
            if (prop.layout === "/admin") {
                if (prop.path === "/fileUpload") {
                    return (
                        <Route
                            path={prop.layout + prop.path}
                            render={props => (
                                <prop.component
                                    {...props}
                                    getFile={this.getFile}
                                    setFile={this.setFile}
                                    setFilename={this.setFilename}
                                    getFileUploadMessage={this.getFileUploadMessage}
                                    setFileUploadMessage={this.setFileUploadMessage}
                                    setColumns={this.setColumns}
                                    setTimeInfo={this.setTimeInfo}
                                />
                            )}
                            key={key}
                        />
                    )
                }
                if (prop.path === "/forecastConfiguration") {
                    return (
                        <Route
                            path={prop.layout + prop.path}
                            render={props => (
                                <prop.component
                                    {...props}
                                    getTimeInfo={this.getTimeInfo}
                                    getColumns={this.getColumns}
                                    getFilename={this.getFilename}
                                    getTargetColumn={this.getTargetColumn}
                                    setTargetColumn={this.setTargetColumn}
                                    setResults={this.setResults}
                                    setTrainingMethod={this.setTrainingMethod}
                                />
                            )}
                            key={key}
                        />
                    )
                }
                if (prop.path === "/results") {
                    return (
                        <Route
                            path={prop.layout + prop.path}
                            render={props => (
                                <prop.component
                                    {...props}
                                    getTrainingMethod={this.getTrainingMethod}
                                    getResults={this.getResults}
                                />
                            )}
                            key={key}
                        />
                    )
                }
                else
                    return (
                        <Route
                            path={prop.layout + prop.path}
                            render={props => (
                                <prop.component
                                    {...props}
                                    getInfo={this.getInfo}
                                    getResults={this.getResults}
                                    getFile={this.getFile}
                                    getColumns={this.getColumns}
                                />
                            )}
                            key={key}
                        />
                    );
            } else {
                return null;
            }
        });
    };

    //! DEFAULT CODE FROM THE TEMPLATE IS BELOW ////////////////////////////////////////////////////////////////////////

    getBrandText = path => {
        for (let i = 0; i < routes.length; i++) {
            if (
                this.props.location.pathname.indexOf(
                    routes[i].layout + routes[i].path
                ) !== -1
            ) {
                return routes[i].name;
            }
        }
        return "Brand";
    };
    handleImageClick = image => {
        this.setState({ image: image });
    };
    handleColorClick = color => {
        this.setState({ color: color });
    };
    handleHasImage = hasImage => {
        this.setState({ hasImage: hasImage });
    };
    handleFixedClick = () => {
        if (this.state.fixedClasses === "dropdown") {
            this.setState({ fixedClasses: "dropdown show-dropdown open" });
        } else {
            this.setState({ fixedClasses: "dropdown" });
        }
    };
    componentDidMount() {
        this.setState({ _notificationSystem: this.refs.notificationSystem });
        var _notificationSystem = this.refs.notificationSystem;
        var color = Math.floor(Math.random() * 4 + 1);
        var level;
        switch (color) {
            case 1:
                level = "success";
                break;
            case 2:
                level = "warning";
                break;
            case 3:
                level = "error";
                break;
            case 4:
                level = "info";
                break;
            default:
                break;
        }
        {/** 
    _notificationSystem.addNotification({
      title: <span data-notify="icon" className="pe-7s-gift" />,
      message: (
        <div>
          Welcome to <b>Light Bootstrap Dashboard</b> - a beautiful freebie for
          every web developer.
        </div>
      ),
      level: level,
      position: "tr",
      autoDismiss: 15
    });
    */}
    }
    componentDidUpdate(e) {
        if (
            window.innerWidth < 993 &&
            e.history.location.pathname !== e.location.pathname &&
            document.documentElement.className.indexOf("nav-open") !== -1
        ) {
            document.documentElement.classList.toggle("nav-open");
        }
        if (e.history.action === "PUSH") {
            document.documentElement.scrollTop = 0;
            document.scrollingElement.scrollTop = 0;
            this.refs.mainPanel.scrollTop = 0;
        }
    }
    render() {
        return (
            <div className="wrapper">
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>Forecasts</title>
                    <link rel="canonical" href="http://mysite.com/example" />
                </Helmet>
                <NotificationSystem ref="notificationSystem" style={style} />
                <Sidebar {...this.props} routes={routes} image={this.state.image}
                    color={this.state.color}
                    hasImage={this.state.hasImage} />
                <div id="main-panel" className="main-panel" ref="mainPanel">
                    <AdminNavbar
                        {...this.props}
                        brandText={this.getBrandText(this.props.location.pathname)}
                    />
                    <Switch>{this.getRoutes(routes)}</Switch>
                    <Footer />
                    {/** 
          <FixedPlugin
            handleImageClick={this.handleImageClick}
            handleColorClick={this.handleColorClick}
            handleHasImage={this.handleHasImage}
            bgColor={this.state["color"]}
            bgImage={this.state["image"]}
            mini={this.state["mini"]}
            handleFixedClick={this.handleFixedClick}
            fixedClasses={this.state.fixedClasses}
          />
          */}
                </div>
            </div>
        );
    }
}

export default Admin;
